using System;

public class UpdateState : GameStateBase
{
    public UpdateState(Action paused, Action resumed, Action updated) : base(paused, resumed, updated)
    {
    }

    public override void Entry()
    {
    }

    public override void Updated()
    {
        OnUpdated?.Invoke();
    }

    public override void Exit()
    {
        
    }
}

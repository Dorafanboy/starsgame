using BurningKnight.PanelManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePanel : PanelBase
{
    [SerializeField] private Button _restart;
    [SerializeField] private Button _backToMenu;
    [SerializeField] private TMP_Text _bestResult;
    
    public override void Init()
    {
    }

    protected override void OnOpened()
    {
        _restart.onClick.AddListener(OnRestartButtonClicked);
        _backToMenu.onClick.AddListener(OnBackToMenuButtonClicked);
        
        _bestResult.text = SaveSystem.GetProgressData().BestScore.ToString();
    }

    protected override void OnClosed()
    {
        _restart.onClick.RemoveListener(OnRestartButtonClicked);
        _backToMenu.onClick.RemoveListener(OnBackToMenuButtonClicked);
    }

    private void OnRestartButtonClicked()
    {
        MainScene.OnReset();
        
        GameManager.ResumeGame();

        Close();
    }
    
    private void OnBackToMenuButtonClicked()
    {
        PanelManager.Instance.OpenPanel<MainMenuPanel>();
        
        MainScene.OnReset();

        Close();
    }
}

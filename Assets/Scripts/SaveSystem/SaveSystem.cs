using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    private static IDataProvider<UserData> _dataProvider;
    private static UserData _userData;

    private void Awake()
    {
        _dataProvider = new PlayerPrefsDataProvider();
        _userData = _dataProvider.Deserialize() ?? new UserData();
    }

    public static ProgressData GetProgressData()
    {
        return _userData.ProgressData;
    }
    
    public static void SaveData()
    {
        _dataProvider.Serialize(_userData);
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            SaveData();
        }
    }
}

using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Star _star;
    [SerializeField] private int _capacity;
    [SerializeField] private float _spawnTime;
    [SerializeField] private float _xOffset;
    
    private Camera _camera;
    private Vector3 _xSpawnPosition;
    private ObjectPool<Star> _pool;
    private IEnumerator _spawn;
    
    private void Awake()
    {
        _camera = Camera.main;
        
        _pool = new ObjectPool<Star>(CreateStar, _capacity);
        
        _spawn = StarSpawner();
    }

    private void OnEnable()
    {
        GameManager.Paused += OnPaused;
        GameManager.Resumed += OnResumed;
    }

    private void OnDisable()
    {
        GameManager.Paused -= OnPaused;
        GameManager.Resumed -= OnResumed;
    }

    private void OnPaused()
    {
        StopCoroutine(_spawn);
    }

    private void OnResumed()
    {
        StartCoroutine(_spawn);
    }

    private void StarDied(Star star)
    {
        star.gameObject.SetActive(false);
        
        _pool.ReturnElement(star);
    }
    
    private IEnumerator StarSpawner()
    {
        var waitForSeconds = new WaitForSeconds(Random.Range(1, _spawnTime));
        
        while (true)
        {
            yield return waitForSeconds;
            
            var takedObject = _pool.TakeObject();

            takedObject.transform.position = GetSpawnPosition(_xSpawnPosition);

            takedObject.Init(StarDied);
        
            takedObject.gameObject.SetActive(true);
        }
    }

    private Star CreateStar()
    {
        _xSpawnPosition = _camera.ScreenToWorldPoint(new Vector3(Screen.width - _xOffset, 0, 0));

        var spawnedStar = Instantiate(_star, GetSpawnPosition(_xSpawnPosition), Quaternion.identity, transform);
        
        spawnedStar.gameObject.SetActive(false);

        return spawnedStar;
    }

    private Vector3 GetSpawnPosition(Vector3 widthSize)
    {
        return new Vector3(Random.Range(widthSize.x, -widthSize.x), transform.position.y, transform.position.z);
    }
}

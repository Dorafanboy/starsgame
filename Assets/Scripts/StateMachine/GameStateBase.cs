using System;

public abstract class GameStateBase 
{
    protected readonly Action OnPaused;
    protected readonly Action OnResumed;
    protected readonly Action OnUpdated;
    
    protected GameStateBase(Action paused, Action resumed, Action updated)
    {
        OnPaused = paused;
        OnResumed = resumed;
        OnUpdated = updated;
    }

    public abstract void Entry();
    public abstract void Updated();
    public abstract void Exit();
}

using System;

[Serializable]
public class UserData : DataBase
{
    public ProgressData ProgressData { get; private set; }

    public UserData()
    {
        ProgressData = new ProgressData();
    }
}

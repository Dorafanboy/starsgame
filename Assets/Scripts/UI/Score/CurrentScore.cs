using UnityEngine;
using UnityEngine.Events;

public class CurrentScore : MonoBehaviour
{
    private int _score;
    public static event UnityAction<int> ScoreChanged;

    private void OnEnable()
    {
        Star.Died += OnStarDied;

        MainScene.Reset += OnReset;
    }

    private void OnDisable()
    {
        Star.Died -= OnStarDied;
        
        MainScene.Reset -= OnReset;
    }

    private void OnStarDied(int reward)
    {
        if (reward < 0)
        {
            return;
        }
        
        _score += reward;
        
        ScoreChanged?.Invoke(_score);
    }

    private void OnReset()
    {
        _score = 0;
        
        ScoreChanged?.Invoke(_score);
    }
}

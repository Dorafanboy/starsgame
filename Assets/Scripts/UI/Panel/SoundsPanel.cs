using BurningKnight.PanelManager;
using UnityEngine;
using UnityEngine.UI;

public class SoundsPanel : PanelBase
{
    [SerializeField] private Slider _sound;
    [SerializeField] private Button _backToMenu;

    private float _soundVolume;

    public override void Init()
    {
        _sound.value = SoundManager.GetSoundVolume();
    }

    protected override void OnOpened()
    {
        _backToMenu.onClick.AddListener(OnBackToMenu);
        _sound.onValueChanged.AddListener(OnSoundValueChanged);
    }

    protected override void OnClosed()
    {
        _backToMenu.onClick.RemoveListener(OnBackToMenu);
        _sound.onValueChanged.RemoveListener(OnSoundValueChanged);
    }

    private void OnBackToMenu()
    {
        SoundManager.SetSoundVolume(_soundVolume);

        //PanelManager.Instance.OpenPanel<MainMenuPanel>();
      
        Close();
    }

    private void OnSoundValueChanged(float value)
    {
        _soundVolume = value;
    }
}

using System;
using BurningKnight.PanelManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : PanelBase
{
    [SerializeField] private TMP_Text _time;
    [SerializeField] private TMP_Text _score;
    [SerializeField] private Button _pause;
    
    public override void Init()
    {
    }

    protected override void OnOpened()
    {
        Timer.TimeOvered += OnTimeOvered;
        CurrentScore.ScoreChanged += OnScoreChanged;
        
        _pause.onClick.AddListener(OnPauseButtonClicked);
    }

    protected override void OnClosed()
    {
        Timer.TimeOvered -= OnTimeOvered;
        CurrentScore.ScoreChanged -= OnScoreChanged;
        
        _pause.onClick.RemoveListener(OnPauseButtonClicked);
    }
    
    private void OnTimeOvered(float waitTime)
    {
        _time.text = Convert.ToInt32(waitTime).ToString();
    }

    private void OnScoreChanged(int score)
    {
        _score.text = score.ToString();
    }
    
    private void OnPauseButtonClicked()
    {
        PanelManager.Instance.OpenPanel<PausePanel>();
                
        GameManager.PauseGame();    
    }
}

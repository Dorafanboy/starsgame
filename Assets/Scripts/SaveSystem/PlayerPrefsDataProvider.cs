using UnityEngine;

public class PlayerPrefsDataProvider : IDataProvider<UserData>
{
    private readonly string _userData = "UserData";
    
    public void Serialize(UserData data)
    {
        PlayerPrefs.SetString(_userData, JsonUtility.ToJson(data));
    }

    public UserData Deserialize()
    {
        return JsonUtility.FromJson<UserData>(PlayerPrefs.GetString(_userData));
    }
}

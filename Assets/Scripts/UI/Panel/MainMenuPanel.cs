using BurningKnight.PanelManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanel : PanelBase
{
    [SerializeField] private Button _startGame;
    [SerializeField] private Button _sound;
    [SerializeField] private Button _exit;
    [SerializeField] private TMP_Text _bestScore;
    
    public override void Init()
    {
    }

    protected override void OnOpened()
    {
        _bestScore.text = SaveSystem.GetProgressData().BestScore.ToString();
        
        _startGame.onClick.AddListener(OnStartGameButtonClicked);
        _sound.onClick.AddListener(OnSoundButtonClicked);
        _exit.onClick.AddListener(OnExitButtonClicked);
    }

    protected override void OnClosed()
    {
        _startGame.onClick.RemoveListener(OnStartGameButtonClicked);
        _sound.onClick.AddListener(OnSoundButtonClicked);
        _exit.onClick.RemoveListener(OnExitButtonClicked);
    }

    private void OnStartGameButtonClicked()
    {
        PanelManager.Instance.OpenPanel<GamePanel>();

        MainScene.OnReset();   

        GameManager.ResumeGame();
    }

    private void OnSoundButtonClicked()
    {
        PanelManager.Instance.OpenPanel<SoundsPanel>();
    }

    private void OnExitButtonClicked()
    {
        Application.Quit();
    }
}

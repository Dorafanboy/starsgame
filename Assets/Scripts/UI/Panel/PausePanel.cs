using BurningKnight.PanelManager;
using UnityEngine;
using UnityEngine.UI;

public class PausePanel : PanelBase
{
    [SerializeField] private Button _resume;
    [SerializeField] private Button _backToMenu;

    public override void Init()
    {
    }

    protected override void OnOpened()
    {
        _resume.onClick.AddListener(OnResumeButtonClicked);
        _backToMenu.onClick.AddListener(OnBackToMenuButtonClicked);
    }

    protected override void OnClosed()
    {
        _resume.onClick.RemoveListener(OnResumeButtonClicked);
        _backToMenu.onClick.RemoveListener(OnBackToMenuButtonClicked);
    }

    private void OnResumeButtonClicked()
    {
        GameManager.ResumeGame();

        Close();
    }

    private void OnBackToMenuButtonClicked()
    {
        PanelManager.Instance.OpenPanel<MainMenuPanel>();
        
        MainScene.OnReset();
        
        Close();
    }
}

using System;
using _ThirdParty.SoundManager;
using UnityEngine;
using UnityEngine.EventSystems;

public class Star : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private int _reward;
    [SerializeField] private SoundPreset _soundPreset;
    [SerializeField] private Transform _view;
    [SerializeField, Range(0f, 7f)] private float _moveSpeed;
    [SerializeField, Range(0f, 2f)] private float _rotateSpeed;
    private Camera _camera;
    private Vector2 _screenPosition;
    private Action<Star> _deleteMethod;
    public static event Action<int> Died;

    public void Init(Action<Star> deleteMethod)
    {
        _camera = Camera.main;
        _screenPosition = _camera.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        _deleteMethod = deleteMethod;
    }

    private void OnEnable()
    {
        GameManager.Updated += OnUpdated;

        MainScene.Reset += OnReset;
    }

    private void OnDisable()
    {
        GameManager.Updated -= OnUpdated;
        
        MainScene.Reset -= OnReset;
    }

    private void OnUpdated()
    {
        transform.Translate(Vector2.down * (_moveSpeed * Time.deltaTime));
        
        _view.transform.Rotate(new Vector3(0, 0, 360f) * (_rotateSpeed * Time.deltaTime));
        
        if (transform.position.y < -_screenPosition.y)
        {
            _deleteMethod(this);
        }
    }

    private void OnReset()
    {
        _deleteMethod(this);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SoundManager.PlaySound(_soundPreset.AudioClip);
        
        Died?.Invoke(_reward);

        _deleteMethod(this);
    }
}

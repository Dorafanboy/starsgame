using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [SerializeField] private float _gameTime;
    private IEnumerator _timer;
    private float _leftTime;
    public static event UnityAction<float> TimeOvered;
    public static event UnityAction GameEnded;

    private void Awake()
    {
        _timer = StartTimer();
        
        _leftTime = _gameTime;
    }

    private void OnEnable()
    {
        GameManager.Paused += OnPaused;
        GameManager.Resumed += OnResumed;

        MainScene.Reset += OnReset;
    }

    private void OnDisable()
    {
        GameManager.Paused -= OnPaused;
        GameManager.Resumed -= OnResumed;
        
        MainScene.Reset -= OnReset;
    }

    private void OnPaused()
    {
        StopCoroutine(_timer);
    }

    private void OnResumed()
    {
        StartCoroutine(_timer);
    }

    private void OnReset()
    {
        _leftTime = _gameTime;
        
        _timer = StartTimer();
        
        StartCoroutine(_timer);
    }
    
    private IEnumerator StartTimer()
    {
        while (_leftTime > 0)
        {
            TimeOvered?.Invoke(_leftTime);
            _leftTime -= Time.deltaTime;
            yield return null;
        }

        GameEnded?.Invoke();
    }
}

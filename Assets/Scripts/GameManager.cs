using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameStateBase _currentStateBase;
    private static PauseState _pauseState;
    private static UpdateState _updatedState;

    public static event Action Paused;
    public static event Action Updated;
    public static event Action Resumed;
    
    private void Awake()
    {
        _pauseState = new PauseState(OnPaused, OnResumed, OnUpdated);
        _updatedState = new UpdateState(OnPaused, OnResumed, OnUpdated);

        _currentStateBase = _pauseState;
    }

    public static void PauseGame()
    {
        SetState(_pauseState);
    }

    public static void ResumeGame()
    {
        SetState(_updatedState);
    }

    private void Update()
    {
        _currentStateBase.Updated();
    }

    private static void SetState(GameStateBase targetState)
    {
        _currentStateBase.Exit();
        _currentStateBase = targetState;
        _currentStateBase.Entry();
    }

    private void OnPaused()
    {
        Paused?.Invoke();
    }

    private void OnResumed()
    {
        Resumed?.Invoke();
    }

    private void OnUpdated()
    {
        Updated?.Invoke();
    }
}

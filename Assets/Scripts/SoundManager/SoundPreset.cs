﻿using UnityEngine;

namespace _ThirdParty.SoundManager
{
    [CreateAssetMenu(fileName = "SoundPreset", menuName = "Audio/SoundPreset")]
    public class SoundPreset : ScriptableObject
    {
        public AudioClip AudioClip;
    }
}
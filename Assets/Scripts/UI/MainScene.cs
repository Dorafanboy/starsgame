using System;
using UnityEngine;

public class MainScene : MonoBehaviour
{
    public static event Action Reset;
    
    private int _bestScore;
    private int _currentScore;

    private void Start()
    {
        _bestScore = SaveSystem.GetProgressData().BestScore;
        
        PanelManager.Instance.OpenPanel<MainMenuPanel>();
    }
  
    private void OnEnable()
    {
        Timer.GameEnded += OnGameEnded;
        CurrentScore.ScoreChanged += OnScoreChanged;
    }

    private void OnDisable()
    {
        Timer.GameEnded -= OnGameEnded;
        CurrentScore.ScoreChanged -= OnScoreChanged;
    }

    private void OnGameEnded()
    {
        if (_currentScore > _bestScore)
        {
            SaveSystem.GetProgressData().BestScore = _currentScore;
            
            SaveSystem.SaveData();
        }
        
        PanelManager.Instance.OpenPanel<EndGamePanel>();
        
        GameManager.PauseGame();
    }

    private void OnScoreChanged(int score)
    {
        _currentScore = score;
    }
    
    public static void OnReset()
    {
        Reset?.Invoke();
    }
}

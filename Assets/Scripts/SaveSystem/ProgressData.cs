using System;
using UnityEngine;

[Serializable]
public class ProgressData : DataBase
{
    [SerializeField] private int _bestScore;

    public int BestScore
    {
        get => _bestScore;
        set
        {
            if (value > _bestScore)
            {
                _bestScore = value;
            }
        }
    }
}

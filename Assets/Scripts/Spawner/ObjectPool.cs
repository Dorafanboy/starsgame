using System;
using System.Collections.Generic;

public class ObjectPool<T>
{
    private Queue<T> _pool = new Queue<T>();
    private readonly Func<T> _createTemplate;

    public ObjectPool(Func<T> createTemplate, int capacity)
    {
        _createTemplate = createTemplate;
        
        for (int i = 0; i < capacity; i++)
        {
            var spawnedElement = createTemplate();
            
            _pool.Enqueue(spawnedElement);
        }
    }

    public T TakeObject()
    {
        if (_pool.Count == 0)
        {
            var spawnedElement = _createTemplate();
            
            _pool.Enqueue(spawnedElement);
        }

        return _pool.Dequeue();
    }

    public void ReturnElement(T returnedElement)
    {
        _pool.Enqueue(returnedElement);
    }
}

using System;

public class PauseState : GameStateBase
{
    public PauseState(Action paused, Action resumed, Action updated) : base(paused, resumed, updated)
    {
    }

    public override void Entry()
    {
        OnPaused?.Invoke();
    }

    public override void Updated()
    {
    }

    public override void Exit()
    {
        OnResumed?.Invoke();
    }
}

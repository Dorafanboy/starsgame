public interface IDataProvider<T>
{
    public void Serialize(T data);
    public T Deserialize();
}
